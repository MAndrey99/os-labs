#include <unistd.h>
#include <iostream>

using namespace std;


int main(int num, char** args) {
    cout << "я внешняя програмка! мой id: " << getpid() << ", а родителя: " << getppid() << endl;
    
    cout << "вот мои аргументы:\n";
    for (int i = 0; i < num; i++) {
        cout << "arg" << i << " " << args[i] << endl;
        sleep(1);
    }

    return 200;
}
