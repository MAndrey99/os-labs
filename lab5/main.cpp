#include <iostream>
#include <unistd.h>
#include <wait.h>

using namespace std;


int main() {
    cout << "Запущена програмка!\n";
    auto child_id = fork(); // создаем копию процесса. child_id это id дочернего(в родительском) или 0(в дочернем)

    if (child_id == 0) {
        // дочерний процесс
        cout << "я дочерний процесс! мой id: " << getpid() << endl << endl;

        // запуск внешней програмки
        if (execl("/home/andrey/hard/cpp_projects/OS_labs/lab5/program_one", "1", "two", "...", NULL) == -1)
            perror("error");

        cout << "\nребенок всё!!!!\n";  // но до этого не дойдет тк он закончится вместе с внешней програмкой
    } else {
        // родительский процесс
        cout << "я родительский процесс! мой id: " << getpid() << ", а ребенка: " << child_id << endl << endl;

        int status; // здесь будет информация о состоянии дочернего потока

        // активное ожидание. WNOHANG означает, что waitpid не блокируется
        while (waitpid(child_id, &status, WNOHANG) != child_id)
            usleep(500000);

        cout << "\nкод возврата дочернего процесса: " << WEXITSTATUS(status) << endl;
    }

    return 0;
}
