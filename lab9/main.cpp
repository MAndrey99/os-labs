#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <string>
#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>


using namespace std;


mqd_t mqdes;
bool finish = false;


void *thread_first(void*) {
    int count = 0;
    string data;

    while(not finish) {
        data = "hello " + to_string(++count);

        mq_send(mqdes, data.c_str(), data.length(), 0);
        cout << data << endl << flush;
        sleep(1);
    }

    return nullptr;
}


int main() {
    mq_unlink("/myqueue");
    mqdes = mq_open("/myqueue", O_CREAT | O_WRONLY, 0644, nullptr);
    if (mqdes == (mqd_t)-1) {
        perror("mq_open");
        return -1;
    }

    pthread_t thread_id;

    pthread_create(&thread_id, nullptr, thread_first, nullptr);
    getchar();
    finish = true;
    pthread_join(thread_id, nullptr);

    mq_close(mqdes);
    mq_unlink("/myqueue");
    return 0;
}
