#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>
#include <string>
#include <mqueue.h>
#include <fcntl.h>
#include <sys/stat.h>

using namespace std;


mqd_t mqdes;
bool finish = false;

void *thread_first(void*) {
    char buf[8193];

    while(not finish) {
        if(auto m_size = mq_receive(mqdes, buf, sizeof(buf) - 1, 0); m_size != -1) {
            buf[m_size] = '\0';
            cout << buf << endl;
        } else
            perror("read");

        sleep(1);
    }

    return nullptr;
}


int main() {
    mqdes = mq_open("/myqueue", O_CREAT | O_RDONLY, 0644, NULL);
    if (mqdes == (mqd_t)-1) {
        perror("mq_open");
        return -1;
    }

    pthread_t thread_id1;

    pthread_create(&thread_id1, nullptr, thread_first, nullptr);

    getchar();
    finish = true;
    pthread_join(thread_id1, nullptr);

    mq_close(mqdes);
    mq_unlink("/myqueue");

    return 0;
}
