#include <iostream>
#include <unistd.h>
#include <pthread.h>
#include <sstream>
#include <fcntl.h>
#include <errno.h>


int FIELDS[2];
bool finish = false;


void* t1f(void*) {
    int count = 0;

    while (not finish) {
        // создаем сообщение
        std::stringstream ss;
        ss << "hello " << count;

        write(FIELDS[1], ss.str().c_str(), ss.str().length()); // записываем его в канал

        usleep(250'000);
        count++;
    }

    return new int(0);
}


void* t2f(void*) {
    char buff[256];

    while (not finish) {
        std::fill(buff, buff+256, 0); // очищаем буффер

        // читаем сообщение в буффер
        while (read(FIELDS[0], buff, 256) == -1 and not finish) {
            if (errno != EAGAIN) {
                // чтение прервано по неизвестной причине(не по тому что должна была пороизойти блокировка)
                perror("impossible read");
            }

            perror("wait");
            usleep(170'000);
        }

        std::cout << buff << std::endl;
    }

    return new int(0);
}


int main() {
    pthread_t thread1, thread2;

    pipe(FIELDS); // создаем канал

    std::cout << fcntl(FIELDS[0], F_GETFL) << std::endl << std::endl;
    fcntl(FIELDS[0], F_SETFL, O_NONBLOCK);

    // создаем потоки
    pthread_create(&thread1, nullptr, t1f, nullptr);
    pthread_create(&thread2, nullptr, t2f, nullptr);

    // ждем нажатия клавиши и завершаем потоки
    getchar();
    finish = true;

    // ожидаем завершение потоков и выводим результат
    int *r1, *r2;
    pthread_join(thread1, (void**)&r1);
    pthread_join(thread2, (void**)&r2);
    std::cout << "\nthread1 return " << *r1
              << "\nthread2 return " << *r2;

    // удаляем коды возврата
    delete r1;
    delete r2;

    // закрываем неименованый канал
    close(FIELDS[0]); // для чтения
    close(FIELDS[1]); // для записи

    return 0;
}
