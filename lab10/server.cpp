#include <iostream>
#include <deque>
#include <sys/socket.h>
#include <sys/resource.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <semaphore.h>
#include <mutex>


pthread_t recv_resp_thread, processing_thread, send_req_thread, connection_thread;

int listen_sok, client_soc;
sockaddr_in addr;
socklen_t addr_len;
bool finish = false;
std::deque<int> request_deque;
std::deque<std::pair<int, rusage>> response_deque;
std::mutex request_deque_mutex, response_deque_mutex, cout_mutex;


void* recvTFunc(void*) {
    int req;

    while (not finish) {
        if (auto n = recv(client_soc, &req, sizeof(req), 0); n != -1) {
            if (n > 0) {
                cout_mutex.lock();
                std::cout << "принято значение " << req << std::endl;
                cout_mutex.unlock();

                request_deque_mutex.lock();
                request_deque.emplace_front(req);
                request_deque_mutex.unlock();
            }
        } else {
            perror("recv");
            sleep(1);
            continue;
        }
    }

    return nullptr;
}


void* respTFunc(void*) {
    std::pair<int, rusage> resp;

    while (not finish) {
        response_deque_mutex.lock();

        if (response_deque.empty()) {
            response_deque_mutex.unlock();
            sleep(1);
        } else {
            resp = response_deque.back();
            response_deque.pop_back();
            response_deque_mutex.unlock();

            if (send(client_soc, &resp, sizeof(resp), 0) == -1) {
                perror("send");
                sleep(1);
            }

            cout_mutex.lock();
            std::cout << "передан ответ на запрос " << resp.first << std::endl;
            cout_mutex.unlock();
        }
    }

    return nullptr;
}


void* processingTFunc(void*) {
    rusage res;
    int arg;

    while (not finish) {
        request_deque_mutex.lock();

        if (request_deque.empty()) {
            request_deque_mutex.unlock();
            sleep(1);
        } else {
            arg = request_deque.back();
            request_deque.pop_back();
            request_deque_mutex.unlock();

            if (getrusage(arg, &res) == -1) {
                perror("getrusage");
                sleep(1);
                continue;
            }

            cout_mutex.lock();
            std::cout << "обработан запрос со значением " << arg << std::endl;
            cout_mutex.unlock();

            response_deque_mutex.lock();
            response_deque.push_front(std::make_pair(arg, res));
            response_deque_mutex.unlock();
        }
    }

    return nullptr;
}


void* waitReqTFunc(void*) {
    while (not finish) {
        if (client_soc = accept(listen_sok, (sockaddr*)&addr, &addr_len); client_soc != -1) {
            pthread_create(&recv_resp_thread, nullptr, recvTFunc, nullptr);
            pthread_create(&processing_thread, nullptr, processingTFunc, nullptr);
            pthread_create(&send_req_thread, nullptr, respTFunc, nullptr);
            return nullptr;
        }
    }

    return nullptr;
}


int main() {
    // создаем сокет
    listen_sok = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_sok == -1) {
        perror("socket");
        return -1;
    }

    // привязываем сокет к адресу и переводим в режим прослушивания
    addr.sin_family = AF_INET;
    addr.sin_port = htons(5555);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr_len = sizeof(addr);
    if (bind(listen_sok, (sockaddr*)&addr, addr_len) < 0) {
        perror("bind");
        close(listen_sok);
        return 0;
    }
    if (listen(listen_sok, 10) == -1) {
        perror("listen");
        close(listen_sok);
        return 0;
    }

    pthread_create(&connection_thread, nullptr, waitReqTFunc, nullptr);
    getchar();
    finish = true;
    pthread_join(connection_thread, nullptr);
    pthread_join(recv_resp_thread, nullptr);
    pthread_join(processing_thread, nullptr);
    pthread_join(send_req_thread, nullptr);

    // закрываем сокеты
    shutdown(client_soc, SHUT_RDWR);
    shutdown(listen_sok, SHUT_RDWR);
    close(client_soc);
    close(listen_sok);

    return 0;
}