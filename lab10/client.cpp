#include <iostream>
#include <deque>
#include <sys/socket.h>
#include <sys/resource.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <semaphore.h>
#include <map>


pthread_t recv_resp_thread, send_req_thread, connection_thread;

int sock;
sockaddr_in addr;
socklen_t addr_len;
bool finish = false;


void* recvRespTFunc(void*) {
    std::pair<int, rusage> resp;
    std::map<int, std::string> who_to_str_map;
    who_to_str_map[RUSAGE_SELF] = "RUSAGE_SELF";
    who_to_str_map[RUSAGE_CHILDREN] = "RUSAGE_CHILDREN";
    who_to_str_map[RUSAGE_THREAD] = "RUSAGE_THREAD";

    while (not finish) {
        if (auto n = recv(sock, &resp, sizeof(resp), 0); n != -1) {
            if (n > 0) { // да, мы можем принять пустоту! (бывает когда вторафя программа разрывает контакт)
                system("clear");
                std::cout << who_to_str_map[resp.first] << std::endl
                          << "user cpu time: " << resp.second.ru_utime.tv_usec << "us\n"
                          << "system cpu time: " << resp.second.ru_stime.tv_usec << "us\n\n";
            }
        } else {
            perror("recv");
            sleep(1);
            continue;
        }
    }

    return nullptr;
}


void* sendReqTFunc(void*) {
    int req;

    while (not finish) {
        sleep(1);
        switch (rand() % 3) {
            case 0:
                req = RUSAGE_SELF;
                break;
            case 1:
                req = RUSAGE_CHILDREN;
            case 2:
                req = RUSAGE_THREAD;
        }

        if (send(sock, &req, sizeof(req), 0) == -1)
            perror("send");
    }

    return nullptr;
}


void* connectionTFunc(void*) {
    while (not finish) {
        if (connect(sock, (sockaddr*)&addr, sizeof(addr)) == -1) {
            perror("connect");
            sleep(1);
            continue;
        }

        pthread_create(&send_req_thread, nullptr, sendReqTFunc, nullptr);
        pthread_create(&recv_resp_thread, nullptr, recvRespTFunc, nullptr);
        return nullptr;
    }

    return nullptr;
}


int main() {
    // создаем сокет
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        perror("socket");
        return -1;
    }

    // привязываем сокет к адресу
    addr.sin_family = AF_INET;
    addr.sin_port = htons(5555);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr_len = sizeof(addr);

    pthread_create(&connection_thread, nullptr, connectionTFunc, nullptr);
    getchar();
    finish = true;
    pthread_join(connection_thread, nullptr);
    pthread_join(recv_resp_thread, nullptr);
    pthread_join(send_req_thread, nullptr);

    // закрываем сокет
    shutdown(sock, SHUT_RDWR);
    close(sock);

    return 0;
}