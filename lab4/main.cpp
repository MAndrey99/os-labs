#include <iostream>
#include <pthread.h>
#include <sstream>
#include <unistd.h>
#include <stdexcept>


template <ulong N>
class PIPE_analog {
    char content[N];
    ulong n = 0;  // заполненность буффера
    ulong in = 0;  // от куда читать
    ulong out = 0;  // от куда записывать

    pthread_mutex_t* mutex;  // мьютекс для синхронизации доступа к content
    pthread_cond_t* cvr;  // условная переменная для чтения
    pthread_cond_t* cvw;  // условная переменная для записи

    void cond_wait(pthread_cond_t* cond) {
        timespec now;
        clock_gettime(CLOCK_REALTIME, &now);
        now.tv_sec += 100;

        if (pthread_cond_timedwait(cond, mutex, &now) != 0)
            throw std::runtime_error("timeout!");
    }

public:
    PIPE_analog() noexcept {
        // выделяем память
        mutex = new pthread_mutex_t;
        cvr = new pthread_cond_t;
        cvw = new pthread_cond_t;

        // инициализируем
        pthread_cond_init(cvr, nullptr);
        pthread_cond_init(cvw, nullptr);
        pthread_mutex_init(mutex, nullptr);
    }

    ~PIPE_analog() {
        pthread_mutex_destroy(mutex);
        pthread_cond_destroy(cvr);
        pthread_cond_destroy(cvw);

        // очищаем память
        delete mutex;
        delete cvr;
        delete cvw;
    }

    /**
     * Читает всё что есть в канале, но не больше чем count
     *
     * @param buff: буффер, в который мы считываем
     * @param count: максимальное количество читаемых байт
     * @return: количество прочитанных байт
     */
    ulong read(char* const buff, ulong count) {
        pthread_mutex_lock(mutex);

        ulong i; // сколько прочитали
        for (i = 0; i != count; i++) {
            if (n == 0) {
                // сигнализируем, что можно продолжить запись
                pthread_cond_signal(cvw);

                // тут автоматически захватывается, а, затем, освобождается после получения сигнала мьютекс
                cond_wait(cvr);
            }

            buff[i] = content[in];
            in = (in + 1) % N;
            n--;
        }

        pthread_cond_signal(cvw);
        pthread_mutex_unlock(mutex);

        return i;
    }

    /**
     * Записывает в канал данные из буффера
     *
     * @param buff: буффер, из которого происходит запись
     * @param count: количество байт, которое надо записать
     */
    void write(const char* buff, ulong count) {
        if (count == 0)
            return;

        buff = buff + count;
        pthread_mutex_lock(mutex);

        do {
            if (n == N) {
                // сигнализируем, что можно продолжить чтение
                pthread_cond_signal(cvr);

                // тут автоматически захватывается, а, затем, освобождается после получения сигнала мьютекс
                cond_wait(cvw);
            }

            content[out] = buff[-count];
            out = (out + 1) % N;
            count--;
            n++;
        } while (count > 0);

        pthread_cond_signal(cvr);
        pthread_mutex_unlock(mutex);
    }

    ulong available_bytes() const {
        return n;
    }
};


bool finish = false;
PIPE_analog<1024> my_pipe;


void* t1f(void*) { // функция первого потока
    int count = 2;

    while (not finish) {
        // создаем сообщение
        std::stringstream ss;
        ss << "hello " << count;

        my_pipe.write(ss.str().c_str(), 256); // записываем его в канал

        count *= 2;
        usleep(400'000);
    }

    return new int(0);
}


void* t2f(void*) { // функция второго потока
    char buff[256];

    while (not finish) {
        std::fill(buff, buff+256, 0); // очищаем буффер

        // ждем пока будет что прочитать
        while (my_pipe.available_bytes() == 0) {
            if (finish) // уже пора закрывать поток
                return new int(0);

            std::cout << "\x1b[33m" << "wait" << "\x1b[0m" << std::endl;
            usleep(300'000);
        }

        try {
            my_pipe.read(buff, 256);  // читаем из канала в буффер
        } catch (std::runtime_error& e) {
            std::cout << e.what();
            return new int(0);
        }

        std::cout << buff << std::endl;  // выводим сообщение
    }

    return new int(0);
}


int main() {
    pthread_t thread1, thread2;

    // создаем потоки
    pthread_create(&thread1, nullptr, t1f, nullptr);
    pthread_create(&thread2, nullptr, t2f, nullptr);

    // ждем нажатия клавиши и завершаем потоки
    getchar();
    finish = true;

    // ожидаем завершение потоков и выводим результат
    int *r1, *r2;

    pthread_join(thread1, (void**)&r1);
    pthread_join(thread2, (void**)&r2);
    std::cout << "\nthread1 return " << *r1
              << "\nthread2 return " << *r2;

    // удаляем коды возврата
    delete r1;
    delete r2;

    return 0;
}
