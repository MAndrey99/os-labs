#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <functional>


struct ThreadArgs {
    std::function<int(sem_t*, bool*)> func;  // функция для запуска в потоке
    int func_id; // номер функции для отображения информации типа запуска и завершения работы
    sem_t* sem;
    bool* finish;

    ThreadArgs(std::function<int(sem_t*, bool*)> func, int func_id, sem_t* sem, bool* finish)
    : func(std::move(func)), func_id(func_id), sem(sem), finish(finish) {}
};


int thread1Fun(sem_t* sem, const bool* const finish) {
    while (not *finish) {
        // раз в 20 милисекунд проверяем можно ли захватит контроль над ресурсом
        while (sem_trywait(sem) != 0) {
            usleep(50'000);

            if (*finish)  // уже надо завершаться
                return 1;
        }

        for (int i = 0; i < 5; i++) {
            std::cout << "1" << std::flush;
            usleep(50'000);
        }

        sem_post(sem); // открываем доступ к ресурсу
        usleep(60'000);
    }

    return 1;
}


int thread2Fun(sem_t* sem, const bool* const finish) {
    while (not *finish) {
        // получаем время через секунду
        timespec now;
        clock_gettime(CLOCK_REALTIME, &now);
        now.tv_sec += 1;

        // пытаемся получить контроль над ресурсом в течении секунды и пишем ошибку при неудаче
        while (sem_timedwait(sem, &now) != 0) {
            perror("error in thread 3");
//            return -1;
            clock_gettime(CLOCK_REALTIME, &now);
            now.tv_sec += 1;
        }

        for (int i = 0; i < 5; i++) {
            std::cout << "2" << std::flush;
            usleep(50'000);
        }

        sem_post(sem); // открываем доступ к ресурсу
        usleep(60'000);
    }

    return 2;
}


// Эта функция служит оберткой для запуска других функций и вывода информации о начале/завершении их работы
void* thread_func(void* args) {
#define arg static_cast<ThreadArgs*>(args)

    std::cout << "func " << arg->func_id << " begin" << std::endl;
    int res = arg->func(arg->sem, arg->finish);
    std::cout << "func " << arg->func_id << " return " << res << std::endl;

    return nullptr;
}


int main() {
    pthread_t t1, t2;  // идентификаторы потоков

    // аргументы потоков
    sem_t sem;
    bool finish;

    // инициализация аргументов потока
    sem_init(&sem, 0, 1);
    finish = false;

    // создание потоков
    auto args1 = ThreadArgs(thread1Fun, 1, &sem, &finish),
         args2 = ThreadArgs(thread2Fun, 2, &sem, &finish);
    pthread_create(&t1, nullptr, thread_func, &args1);  // создаем первый поток
    pthread_create(&t2, nullptr, thread_func, &args2);  // создаем второй поток

    getchar(); // ожидание нажатия клавиши
    finish = true; // ставлю флаг, что потоки должены закрыться

    // ожидаем завершение потоков
    pthread_join(t1, nullptr);
    pthread_join(t2, nullptr);

    sem_destroy(&sem);  // уничтожаем семофор
    return 0;
}
