#include <stdio.h>
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string>
#include <string.h>
#include <semaphore.h>
#include <errno.h>
#include <unistd.h>


using namespace std;

int myfifo;
bool finish = false;

void* func(void*) {
    char buff[128];

    while(not finish) {
        buff[read(myfifo, &buff, sizeof(buff) - 1)] = '\0';
        cout << buff << endl;
    }

    return nullptr;
}

int main() {
    pthread_t id;

    // создаем именованный канал
    mkfifo("myfifo", 0777);
    myfifo = open("myfifo", O_RDONLY);
    if (myfifo == -1) {
        perror("open");
        return -1;
    }

    pthread_create(&id, nullptr, func, nullptr);

    getchar();
    finish = true;
    pthread_join(id, nullptr);

    // удаляем именованный канал
    close(myfifo);
    unlink("myfifo");

    return 0;
}
