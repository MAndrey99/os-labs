#include <stdio.h>
#include <pthread.h>
#include <iostream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string>
#include <string.h>
#include <semaphore.h>
#include <sstream>

using namespace std;

int myfifo;
bool finish = false;

void* func(void*) {
    int count = 0;
    string data;

    while(not finish) {
        data = "hello " + to_string(++count);

        if (write(myfifo, data.c_str(), data.length()) == -1) // передаем сообщение
            perror("write");
        else
            cout << data << endl; // выводим переданное сообщение

        sleep(1);
    }

    return nullptr;
}


int main() {
    unlink("myfifo");

    pthread_t id;

    // создаем именованный канал
    mkfifo("myfifo", 0777);
    myfifo = open("myfifo", O_WRONLY);
    if (myfifo == -1) {
        perror("open");
        return -1;
    }

    pthread_create(&id, nullptr, func, nullptr);

    getchar();
    finish = true;
    pthread_join(id, nullptr);

    // удаляем именованный канал
    close(myfifo);
    unlink("myfifo");

    return 0;
}