#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <cstring>


void* thread1Fun(void* exit_f) {
    while (not *((bool*)exit_f)) {
        std::cout << "thread 1!!!\n";
        sleep(1);
    }

    return new int(1);
}

void* thread2Fun(void* exit_f) {
    while (not *((bool*)exit_f)) {
        std::cout << "thread 2!!!\n";
        sleep(2);
    }

    pthread_exit(new int(2));  // альтернативный вариант завершения потока
}


int main() {
    pthread_t t1, t2;  // идентификаторы потоков
    bool t1_exit_flag(false), t2_exit_flag(false); // флаги завершения потоков

    // создание потоков
    pthread_create(&t1, nullptr, thread1Fun, &t1_exit_flag);
    pthread_create(&t2, nullptr, thread2Fun, &t2_exit_flag);

    getchar(); // ожидание нажатия клавиши
    t1_exit_flag = t2_exit_flag = true; // обозначаем что все потоки должны завершиться

    // ожидаем завершение потоков и читаем результат
    int *t1_return_code, *t2_return_code;  // указатели на данные, которые передают потоки потоки при завершении
    pthread_join(t1, (void**)&t1_return_code);
    pthread_join(t2, (void**)&t2_return_code);

    // выводим коды завершения потоков
    std::cout << "\nthread1 return " << *t1_return_code
              << "\nthread2 return " << *t2_return_code;

    // очищаем выделенную память
    delete t1_return_code;
    delete t2_return_code;

    return 0;
}
