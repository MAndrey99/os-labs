#include <iostream>
#include <fstream>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <cstring>

using namespace std;

bool flag = true;
sem_t *r_sem, *w_sem;
int* shared = (int*)-1;
int fd = -1;


void* read_func(void*) {
    int var;

    while (flag) {
        sem_wait(w_sem);
        var = *shared;
        cout << var << endl;
        sem_post(r_sem);
    }

    return nullptr;
}


void* write_func(void*) {
    int var;

    while (flag) {
        var = rand() % 100;
        cout << var << endl;
        *shared = var;
        sem_post(w_sem);
        sem_wait(r_sem);
        sleep(2);
    }

    return nullptr;
}


void final() {
    // уничтожаем семафоры
    if (r_sem != SEM_FAILED) {
        sem_close(r_sem);
        sem_unlink("my_sem_r");
    }
    if (w_sem != SEM_FAILED) {
        sem_close(w_sem);
        sem_unlink("my_sem_w");
    }

    // уничтожаем разделяемую память
    if (fd != -1) {
        if (shared != (int*)-1)
            munmap(shared, sizeof(int));

        close(fd);
        shm_unlink("my_mem");
    }
}


int main(int argc, char** argv) {
//    srand(time(nullptr));

    // открываем семафоры
    r_sem = sem_open("my_sem_r", O_CREAT, 0644, 0);
    w_sem = sem_open("my_sem_w", O_CREAT, 0644, 0);
    if (r_sem == SEM_FAILED or w_sem == SEM_FAILED) {
        perror("не удалось создать именованый семафор");
        final();
        return -1;
    }

    // открываем разделяемую память
    fd = shm_open("my_mem", O_CREAT | O_RDWR, 0644);
    if (fd == -1) {
        perror("не удалось создать разделяемую память");
        return -1;
    }
    if (ftruncate(fd, sizeof(int)) == -1) {
        perror("truncate");
        final();
        return 1;
    }
    shared = static_cast<int *>(mmap(0, sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0));
    if (shared == (int*)-1) {
        perror("mmap");
        final();
        return 1;
    }

    // создаем поток
    pthread_t thread;
    pthread_create(&thread, nullptr,  (argc == 2 and strcmp(argv[1], "r") == 0) ? read_func : write_func, nullptr);

    // ожидаем нажатия клавишм и завершаем выполнение потока
    getchar();
    flag = false;
    pthread_join(thread, nullptr);

    final();
    return 0;
}
