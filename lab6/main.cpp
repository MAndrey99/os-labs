#include <iostream>
#include <fstream>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <fcntl.h>

using namespace std;

bool flag = true;
sem_t *sem;


void* thread_func(void*) {
    ofstream fout("file.txt", ios_base::app);

    while (flag) {
        timespec now;
        clock_gettime(CLOCK_REALTIME, &now);
        now.tv_sec += 1;

        while (sem_timedwait(sem, &now) != 0) {
            perror("");

            if (!flag)
                return nullptr;

            clock_gettime(CLOCK_REALTIME, &now);
            now.tv_sec += 1;
        }

        for (int j = 0; j < 10; j++) {
            fout << SYNBOL << flush;
            usleep(1'000);
        }
        sem_post(sem);
        usleep(5'000);
    }

    fout.close();
    return nullptr;
}


int main() {
    sem = sem_open("Semm", O_CREAT, 0644, 1);
    if (sem == SEM_FAILED) {
        perror("не удалось создать именованый семафор");
        return -1;
    }

    pthread_t thread;
    pthread_create(&thread, nullptr, thread_func, nullptr);

    getchar();
    flag = false;
    pthread_join(thread, nullptr);

    sem_close(sem);
    sem_unlink("Semm");

    return 0;
}
