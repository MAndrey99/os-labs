cmake_minimum_required(VERSION 3.14)
project(lab6)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-pthread")

add_executable(main main.cpp)
target_compile_definitions(main PUBLIC -D -DSYNBOL='1')

add_executable(one main.cpp)
target_compile_definitions(one  PUBLIC -D -DSYNBOL='2')
